package com.devdream.cloud.authserver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.devdream.cloud.authserver.userdetails.IUserDetailsService;
import com.devdream.cloud.authserver.userdetails.User;
import com.devdream.service.dispatcher.Command;

@RestController
public class SignUpCommandController {

	IUserDetailsService userDetailsService;

	@Autowired
	public SignUpCommandController(IUserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@RequestMapping(value = "/sign-up", method = RequestMethod.POST)
	@SuppressWarnings({ "rawtypes" })
	public ResponseEntity registerClient(@RequestBody Command registerClientCommand) {

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

		userDetailsService.addUserDetails(new User(registerClientCommand.getPayload().get("username"), registerClientCommand.getPayload().get("password"), true, true, true, true,
				authorities));

		return new ResponseEntity(HttpStatus.OK);
	}

}
