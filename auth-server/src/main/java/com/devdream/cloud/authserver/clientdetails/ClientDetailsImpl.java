package com.devdream.cloud.authserver.clientdetails;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

@Document
public class ClientDetailsImpl implements ClientDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6177735273166145314L;
	
	private final String clientId;
	private final Set<String> resourceIds;
	private final boolean isSecretRequired;
	private final String clientSecret;
	private final boolean isScoped;
	private final Set<String> scope;
	private final Set<String> authorizedGrantTypes;
	private final Set<String> registeredRedirectUri;
	private final Collection<GrantedAuthority> authorities;
	private final Integer accessTokenValiditySeconds;
	private final Integer refreshTokenValiditySeconds;
	private final Map<String, Object> additionalInformation;
	
	public ClientDetailsImpl(String clientId, Set<String> resourceIds,
			boolean isSecretRequired, String clientSecret, boolean isScoped,
			Set<String> scope, Set<String> authorizedGrantTypes,
			Set<String> registeredRedirectUri,
			Collection<GrantedAuthority> authorities,
			Integer accessTokenValiditySeconds,
			Integer refreshTokenValiditySeconds,
			Map<String, Object> additionalInformation) {
		super();
		this.clientId = clientId;
		this.resourceIds = resourceIds;
		this.isSecretRequired = isSecretRequired;
		this.clientSecret = clientSecret;
		this.isScoped = isScoped;
		this.scope = scope;
		this.authorizedGrantTypes = authorizedGrantTypes;
		this.registeredRedirectUri = registeredRedirectUri;
		this.authorities = authorities;
		this.accessTokenValiditySeconds = accessTokenValiditySeconds;
		this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
		this.additionalInformation = additionalInformation;
	}

	@Override
	public String getClientId() {
		return clientId;
	}

	@Override
	public Set<String> getResourceIds() {
		return resourceIds;
	}

	@Override
	public boolean isSecretRequired() {
		return isSecretRequired;
	}

	@Override
	public String getClientSecret() {
		return clientSecret;
	}

	@Override
	public boolean isScoped() {
		return isScoped;
	}

	@Override
	public Set<String> getScope() {
		return scope;
	}

	@Override
	public Set<String> getAuthorizedGrantTypes() {
		return authorizedGrantTypes;
	}

	@Override
	public Set<String> getRegisteredRedirectUri() {
		return registeredRedirectUri;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public Integer getAccessTokenValiditySeconds() {
		return accessTokenValiditySeconds;
	}

	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return refreshTokenValiditySeconds;
	}

	@Override
	public boolean isAutoApprove(String scope) {
		return false;
	}

	@Override
	public Map<String, Object> getAdditionalInformation() {
		return additionalInformation;
	}

}
