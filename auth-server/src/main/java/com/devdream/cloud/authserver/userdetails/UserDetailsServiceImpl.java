package com.devdream.cloud.authserver.userdetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements IUserDetailsService {

	UserRepository userMongoRepository;
	PasswordEncoder passwordEncoder;

	@Autowired
	public UserDetailsServiceImpl(UserRepository userDetailsMongoRepository, PasswordEncoder passwordEncoder) {
		this.userMongoRepository = userDetailsMongoRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userMongoRepository.findOne(username);
	}

	@Override
	public void addUserDetails(UserDetails userDetails) {

		userMongoRepository.insert(new User(userDetails.getUsername(), passwordEncoder.encode(userDetails.getPassword()), userDetails.isAccountNonExpired(), userDetails
				.isAccountNonLocked(), userDetails.isCredentialsNonExpired(), userDetails.isEnabled(), userDetails.getAuthorities()));
	}

	@Override
	public void updateUserDetails(UserDetails userDetails) {
		userMongoRepository.save(new User(userDetails.getUsername(), passwordEncoder.encode(userDetails.getPassword()), userDetails.isAccountNonExpired(), userDetails
				.isAccountNonLocked(), userDetails.isCredentialsNonExpired(), userDetails.isEnabled(), userDetails.getAuthorities()));
	}

	@Override
	public void removeUserDetails(String username) {
		userMongoRepository.delete(username);
	}

}
