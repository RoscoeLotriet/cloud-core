package com.devdream.cloud.authserver.userdetails;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface IUserDetailsService extends UserDetailsService,
		UserDetailsRegistrationService {

}
