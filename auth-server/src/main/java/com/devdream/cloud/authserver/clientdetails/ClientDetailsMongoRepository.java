package com.devdream.cloud.authserver.clientdetails;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.oauth2.provider.ClientDetails;

public interface ClientDetailsMongoRepository extends
		MongoRepository<ClientDetails, String> {

}
