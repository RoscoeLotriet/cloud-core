package com.devdream.cloud.authserver;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class OAuthController {

	@RequestMapping(value = "/oauth/confirm_access", method = RequestMethod.GET)
	public ModelAndView confirmAccess() {
		return new ModelAndView("confirm-access.html");
	}

}
