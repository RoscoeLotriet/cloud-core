package com.devdream.cloud.authserver.clientdetails;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientAlreadyExistsException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.ClientRegistrationService;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.stereotype.Service;

@Service
public class ClientDetailsServiceImpl implements ClientDetailsService,
		ClientRegistrationService {

	private final ClientDetailsMongoRepository clientDetailsMongoRepository;

	@Autowired
	public ClientDetailsServiceImpl(
			ClientDetailsMongoRepository clientDetailsMongoRepository) {
		super();
		this.clientDetailsMongoRepository = clientDetailsMongoRepository;
	}

	@Override
	public ClientDetails loadClientByClientId(String clientId)
			throws ClientRegistrationException {
		return clientDetailsMongoRepository.findOne(clientId);
	}

	@Override
	public void addClientDetails(ClientDetails clientDetails)
			throws ClientAlreadyExistsException {
		clientDetailsMongoRepository.insert(clientDetails);
	}

	@Override
	public void updateClientDetails(ClientDetails clientDetails)
			throws NoSuchClientException {
		clientDetailsMongoRepository.save(clientDetails);

	}

	@Override
	public void updateClientSecret(String clientId, String secret)
			throws NoSuchClientException {

	}

	@Override
	public void removeClientDetails(String clientId)
			throws NoSuchClientException {
		clientDetailsMongoRepository.delete(clientId);

	}

	@Override
	public List<ClientDetails> listClientDetails() {
		return clientDetailsMongoRepository.findAll();
	}

}
