package com.devdream.cloud.authserver.userdetails;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserDetailsRegistrationService {

	void addUserDetails(UserDetails userDetails);

	void updateUserDetails(UserDetails userDetails);

	void removeUserDetails(String username);

}
