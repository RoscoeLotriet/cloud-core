import App from './app.js';
new App().start();

require('devdream-ui-core/lib/polyfill.js')
require('devdream-ui-core/lib/polyfill.css')
require('devdream-material-design-lite/dist/material.css')
require('./style.less');
// only load style when using webpack
/* istanbul ignore if  */
