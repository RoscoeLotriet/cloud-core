import React from 'react'
import {CommonLayout} from 'devdream-ui-common'
import {
    NavGroup,
    NavLink,
    Badge,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    CardActions,
    Cell,
    Grid,
    LayoutSpacer,
    Select,
    TextField,
    Checkbox,
    Button,
    CommandValueLink
} from 'devdream-ui-core'
import cookie from 'cookie'
const cookies = cookie.parse(document.cookie)



class Template extends React.Component {

    constructor(props){
        super(props);
        this.state = {};
        this.state['sign-up'] = {};
    }

    componentDidMount(){

    }

    logIn = () => {
        console.log(this.refs.loginForm)
        this.refs.loginForm.getDOMNode().submit();
    }

    render(){
    
    	const navGroups = ([
            // <NavGroup>
            //     <NavLink>
            //         <Badge value={99}>
            //             <b>A menu badge</b>
            //         </Badge>
            //     </NavLink>
            // </NavGroup>
        ]);

        const error = this.props.error;
        console.log(this.state)

        return (

            <CommonLayout fixedHeader pageTitle="Your Brand Here" drawerTitle="Your Brand Here" navGroups={navGroups} >
            <Grid>
                <Cell desktopSize={6}>
                    <form action="login" method="post" ref="loginForm">
                <Card className="mdl-shadow--4dp command-card">
                        <CardTitle className="mdl-card--expand">
                          <CardTitleText>Log In&nbsp;<i className="material-icons">lock</i></CardTitleText>
                        </CardTitle>
                        <CardSupportingText>
                            
                            <Grid>
                                <Cell desktopSize={6}>
                                    <TextField label="Username" name="username" floatingLabel/>
                                </Cell>
                                
                            </Grid>
                            <Grid>
                                <Cell desktopSize={6}>
                                    <TextField label="Password" name="password" floatingLabel/>
                                </Cell>
                            </Grid>
                        </CardSupportingText>
                        <CardActions className="mdl-card--border">
                            <Button raised colored ripple onClick={this.logIn}>Log In</Button>
                            {error ? <span className="mdl-color-text--accent"> Invalid credentials.</span> : null}
                        </CardActions>
                </Card>
                </form>
                </Cell>
                <Cell desktopSize={6}>
                    <Card className="mdl-shadow--4dp command-card">
                        <CardTitle className="mdl-card--expand">
                          <CardTitleText>Sign Up&nbsp;<i className="material-icons">account_box</i></CardTitleText>
                        </CardTitle>
                        <CardSupportingText>
                            
                            <Grid>
                                <Cell desktopSize={6}>
                                    <TextField label="Username" name="username" floatingLabel valueLink={CommandValueLink(this, 'sign-up', 'username')}/>
                                </Cell>
                                <Cell desktopSize={6}>
                                    <Select label="Role" name="role" floatingLabel options={[{value: 'ROLE_USER', name: 'User'}]} valueLink={CommandValueLink(this, 'sign-up', 'role')} addBlank/>
                                </Cell>
                            </Grid>
                            <Grid>
                                <Cell desktopSize={6}>
                                    <TextField label="Password" name="password" floatingLabel valueLink={CommandValueLink(this, 'sign-up', 'password')}/>
                                </Cell>
                            </Grid>
                        </CardSupportingText>
                        <CardActions className="mdl-card--border">
                            <Button raised colored ripple accent onClick={this.props.signUp.bind(this, this.state['sign-up'])}>Sign Up</Button>
                        </CardActions>
                </Card>
                </Cell>
            </Grid>
                
                
            </CommonLayout>

        );

    }
};

export default Template;
