// import all routes
import Index from './pages/index/index';
import NotFound from './pages/not-found/index';

// create route array
const routes = [
	Index,
    NotFound
];

export default routes;
