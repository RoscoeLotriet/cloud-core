import {ServiceDispatcher} from 'devdream-service-dispatcher'
const dispatcher = new ServiceDispatcher({serviceGateway: 'http://localhost:9999/'})
import _ from 'underscore'

export default class Store {

	constructor(){

		this.clientContactViews = [];
		this.clientCaptured = false;
		this.clientView = {};

		this.createCommandSubscription('sign-up', (data) => {
			this.signedUp = true;
			//this.OnChangeHandler();
		})
    	
	}

	registerOnChange(handler){
		console.log(handler)
		this.OnChangeHandler = handler;
	}

	createQuerySubscription(topic, handler){
		return dispatcher.getQueryResponseTopic(topic).subscribe(handler)
	}

	createCommandSubscription = (topic, handler) => {
		return dispatcher.getCommandResponseTopic(topic).subscribe(handler)
	}

	getState(){
		return { 
			pageObject: {

			}
		};
	};

	command(serviceId, commandType, commandProperties){
		dispatcher.dispatchCommand(serviceId, commandType, commandProperties)
	}

	query(serviceId, queryType, queryProperties){
		dispatcher.dispatchQuery(serviceId, queryType, queryProperties);
	}

}