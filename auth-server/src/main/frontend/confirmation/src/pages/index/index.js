import React from 'react'
import {DefaultRoute} from 'react-router'
import Template from './template'
import Store from './store'
const PageStore = new Store()
// only load style when using webpack
/* istanbul ignore if  */
require('./style.less');


class IndexPage extends React.Component {

    constructor(){
        super();
        this.state = {};
        this.state.pageObject = {};
    }

	getStateFromStores(){
		return PageStore.getState()
	}

    componentDidMount(){

    }

    signUp = (payload) => {
       

        const sub = PageStore.createCommandSubscription('sign-up', (data) => {
             //PageStore.query('quotation-service', 'capture-client', {clientId: payload['clientId']});
             sub.dispose();
        })

        PageStore.command('uaa','sign-up', payload)
        
       
    }

    render(){

    	return <Template pageObject={this.state.pageObject} signUp={this.signUp}/>;
    }

}

const IndexRoute = React.createElement(DefaultRoute, {name: 'home', key: 'route_default', handler: IndexPage});

export default IndexRoute;
