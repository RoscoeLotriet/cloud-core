import React from 'react'
import {CommonLayout} from 'devdream-ui-common'
import {
    NavGroup,
    NavLink,
    Badge,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    CardActions,
    Cell,
    Grid,
    LayoutSpacer,
    Select,
    TextField,
    Checkbox,
    Button,
    CommandValueLink
} from 'devdream-ui-core'
import cookie from 'cookie'
const cookies = cookie.parse(document.cookie)



class Template extends React.Component {

    constructor(props){
        super(props);
        this.state = {};
        this.state['sign-up'] = {};
    }

    componentDidMount(){

    }

    approve = () => {

        this.refs.approvalForm.getDOMNode().submit();
    }

    deny = () => {

        this.refs.denialForm.getDOMNode().submit();
    }

    render(){
    
    	const navGroups = ([
            // <NavGroup>
            //     <NavLink>
            //         <Badge value={99}>
            //             <b>A menu badge</b>
            //         </Badge>
            //     </NavLink>
            // </NavGroup>
        ]);


        return (

            <CommonLayout fixedHeader pageTitle="Your Brand Here" drawerTitle="Your Brand Here" navGroups={navGroups} >
            <Grid>
                <Cell desktopSize={12}>

                <Card className="mdl-shadow--4dp command-card">
                        <CardTitle className="mdl-card--expand">
                          <CardTitleText>Confirmation&nbsp;<i className="material-icons">verified_user</i></CardTitleText>
                        </CardTitle>
                        <CardSupportingText>
                            
                            

                            <Grid>
                                <Cell desktopSize={12}>
                                    <h4>Do you authorize this app to access your protected resources?</h4>
                                    <form ref="approvalForm" action='/uaa/oauth/authorize' method='post'>
                                        <input name='user_oauth_approval' value='true' type='hidden'/>
                                        <input name='scope.openid' value='true' type='hidden'/>
                                        <input name='authorize' value='Authorize' type='hidden'/>
                                    </form>
                                    <form ref='denialForm' action='/uaa/oauth/authorize' method='post'>
                                        <input name='user_oauth_approval' value='false' type='hidden'/>
                                    </form>
                                </Cell>
                            </Grid>
                            
                        </CardSupportingText>
                        <CardActions className="mdl-card--border">
                            <Button raised colored ripple onClick={this.approve}>Approve</Button>
                            <span>&nbsp;</span>
                            <Button raised colored ripple accent onClick={this.deny}>Deny</Button>
                        </CardActions>
                </Card>
  
                </Cell>
                
            </Grid>
                
                
            </CommonLayout>

        );

    }
};

export default Template;
