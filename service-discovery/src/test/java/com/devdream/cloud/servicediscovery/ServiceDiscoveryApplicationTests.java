package com.devdream.cloud.servicediscovery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.devdream.cloud.servicediscovery.ServiceDiscoveryApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServiceDiscoveryApplication.class)
public class ServiceDiscoveryApplicationTests {

	@Test
	public void contextLoads() {
	}

}
