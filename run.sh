#! /bin/bash -e

echo starting services

SERVICE_COMMAND_ARRAY=(
	'service-discovery bootRun'
	'auth-server startLoginWebpackServer'
	'auth-server startConfirmationWebpackServer'
	'auth-server bootRun'
	'api-gateway bootRun'
)

COMMAND_PREFIX=" --tab-with-profile=Roscoe -e './gradlew -p ./"
COMMAND_SUFFIX="'"
TERMINAL_COMMAND=''

declare -a TERMINAL_COMMAND_ARRAY
#for SERVICE_COMMAND in "${SERVICE_COMMAND_ARRAY[@]}"
#do
#   echo "$SERVICE_COMMAND"
#   $COMMAND_PREFIX$SERVICE_COMMAND
#done

for ((i=0; i < ${#SERVICE_COMMAND_ARRAY[@]}; i++))
do
  echo ${SERVICE_COMMAND_ARRAY[$i]}
  TERMINAL_COMMAND_ARRAY[$i]=$COMMAND_PREFIX${SERVICE_COMMAND_ARRAY[$i]}$COMMAND_SUFFIX
done

GNOME_TERMINAL='gnome-terminal'
echo $GNOME_TERMINAL${TERMINAL_COMMAND_ARRAY[@]}
eval $GNOME_TERMINAL${TERMINAL_COMMAND_ARRAY[@]}

echo services started
