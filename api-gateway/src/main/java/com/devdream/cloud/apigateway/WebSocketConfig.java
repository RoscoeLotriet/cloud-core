package com.devdream.cloud.apigateway;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

	@Override
	public void configureClientInboundChannel(ChannelRegistration registration) {
		super.configureClientInboundChannel(registration);
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {

		// registry.enableSimpleBroker("/topic", "/queue");
		registry.enableStompBrokerRelay("/topic", "/queue", "/exchange");

	};

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		// use the /stomp endpoint
		registry.addEndpoint("/stomp").setAllowedOrigins("*").withSockJS();

	}

}
