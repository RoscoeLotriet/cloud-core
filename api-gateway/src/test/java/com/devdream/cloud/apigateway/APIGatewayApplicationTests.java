package com.devdream.cloud.apigateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.devdream.cloud.apigateway.APIGatewayApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = APIGatewayApplication.class)
@WebAppConfiguration
public class APIGatewayApplicationTests {

	@Test
	public void contextLoads() {
	}

}